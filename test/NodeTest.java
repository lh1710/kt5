
import static org.junit.Assert.*;
import org.junit.Test;
// import java.util.*;

/** Testklass.
 * @author Jaanus
 */
public class NodeTest {

   @Test (timeout=1000)
   public void testParsePostfix() { 
      String s = "(B1,C,D)A";
      Node t = Node.parsePostfix (s);
      System.out.println(t.leftParentheticRepresentation());
      String r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "A(B1,C,D)", r);
      s = "(((2,1)-,4)*,(69,3)/)+";
      t = Node.parsePostfix (s);
      r = t.leftParentheticRepresentation();
      System.out.println(r);
      assertEquals ("Tree: " + s, "+(*(-(2,1),4),/(69,3))", r);
   }
   @Test
   public void testxml1() {
      String s = "((((E)D)C)B)A";
      Node t = Node.parsePostfix (s);
      assertEquals("<L1> A\n" +
              "\t<L2> B\n" +
              "\t\t<L3> C\n" +
              "\t\t\t<L4> D\n" +
              "\t\t\t\t<L5> E </L5>\n" +
              "\t\t\t</L4>\n" +
              "\t\t</L3>\n" +
              "\t</L2>\n" +
              "</L1>\n"
              , t.getXmlFormat(1));
   }

   @Test
   public void testxml2() {
      String s = "((C)B,(E,F)D,G)A";
      Node t = Node.parsePostfix (s);
      System.out.println(t.getXmlFormat(1));
      assertEquals("<L1> A\n" +
              "\t<L2> B\n" +
              "\t\t<L3> C </L3>\n" +
              "\t</L2>\n" +
              "\t<L2> D\n" +
              "\t\t<L3> E </L3>\n" +
              "\t\t<L3> F </L3>\n" +
              "\t</L2>\n" +
              "\t<L2> G </L2>\n" +
              "</L1>\n", t.getXmlFormat(1));
   }

   @Test
   public void testxml3() {
      String s = "((C,(E)D)B,F)A";
      Node t = Node.parsePostfix (s);
      assertEquals("<L1> A\n" +
              "\t<L2> B\n" +
              "\t\t<L3> C </L3>\n" +
              "\t\t<L3> D\n" +
              "\t\t\t<L4> E </L4>\n" +
              "\t\t</L3>\n" +
              "\t</L2>\n" +
              "\t<L2> F </L2>\n" +
              "</L1>\n", t.getXmlFormat(1));
   }

   @Test
   public void testParsePostfixAndLeftParentheticRepresentation1() {
      String s = "((((E)D)C)B)A";
      Node t = Node.parsePostfix (s);
      String a = "((C,(E)D)B,F)A";
      Node b = Node.parsePostfix(a);
      System.out.println(b.getXmlFormat(1));
      String r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "A(B(C(D(E))))", r);
      s = "((C,(E)D)B,F)A";
      t = Node.parsePostfix (s);
      r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "A(B(C,D(E)),F)", r);
   }

   @Test (timeout=1000)
   public void testParsePostfixAndLeftParentheticRepresentation2() {
      String s = "(((512,1)-,4)*,(-6,3)/)+";
      Node t = Node.parsePostfix (s);
      String r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "+(*(-(512,1),4),/(-6,3))", r);
      s = "(B,C,D,E,F)A";
      t = Node.parsePostfix (s);
      r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "A(B,C,D,E,F)", r);
      s = "((1,(2)3,4)5)6";
      t = Node.parsePostfix (s);
      r = t.leftParentheticRepresentation();
       assertEquals ("Tree: " + s, "6(5(1,3(2),4))", r);
   }

   @Test (timeout=1000)
   public void testSingleRoot() {
      String s = "ABC";
      Node t = Node.parsePostfix (s);
      String r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "ABC", r);
      s = ".Y.";
      t = new Node (s, null, null);
      r = t.leftParentheticRepresentation();
      assertEquals ("Single node" + s, s, r);
   } 

   @Test (expected=RuntimeException.class)
   public void testSpaceInNodeName() {
      Node root = Node.parsePostfix ("A B");
   }

   @Test (expected=RuntimeException.class)
   public void testTwoCommas() {
      Node t = Node.parsePostfix ("(B,,C)A");
   }

   @Test (expected=RuntimeException.class)
   public void testEmptySubtree() {
      Node root = Node.parsePostfix ("()A");
   }

   @Test (expected=RuntimeException.class)
   public void testInputWithBracketsAndComma() {
      Node t = Node.parsePostfix ("( , ) ");
   }

   @Test (expected=RuntimeException.class)
   public void testInputWithoutBrackets() {
      Node t = Node.parsePostfix ("A,B");
   }

   @Test (expected=RuntimeException.class)
   public void testInputWithDoubleBrackets() {
      Node t = Node.parsePostfix ("((C,D))A");
   }

   @Test (expected=RuntimeException.class)
   public void testComma1() {
      Node root = Node.parsePostfix ("(,B)A");
   }

   @Test (expected=RuntimeException.class)
   public void testComma2() {
      Node root = Node.parsePostfix ("(B)A,(D)C");
   }

   @Test (expected=RuntimeException.class)
   public void testComma3() {
      Node root = Node.parsePostfix ("(B,C)A,D");
   }

   @Test (expected=RuntimeException.class)
   public void testTab1() {
      Node root = Node.parsePostfix ("\t");
   }

   @Test (expected=RuntimeException.class)
   public void testWeirdBrackets() {
      Node root = Node.parsePostfix (")A(");
   }

}

