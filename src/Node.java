import java.util.Stack; // https://git.wut.ee/i231/home5/src/branch/master/src/Node.java

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }

   public static Node parsePostfix (String s) {
      if (s.contains(",,")) {
         throw new RuntimeException("Cannot have double commas in " + s);
      } else if (s.contains("()")) {
         throw new RuntimeException("Cannot have empty subtree in " + s);
      } else if (s.contains("((") && s.contains("))")) {
         throw new RuntimeException("Cannot have double brackets in " + s);
      } else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) {
         throw new RuntimeException("Cannot have two roots in " + s);
      }

      if (!s.contains("(") && !s.contains(")")) {
         if (s.contains(" ")) {
            throw new RuntimeException("Cannot have space in " + s);
         }
         else if (s.contains("\t")) {
            throw new RuntimeException("Cannot have tab in " + s);
         }
      }
      String[] x = s.split("\\(", 2);
      if (x.length == 2) {
         if(x[0].contains(" ")) {
            throw new RuntimeException("Cannot have space in " + s);
         }
      }

      String[] y = s.split("\\)", 2);
      if (y.length == 2) {
         if(y[1].contains(" ")) {
            throw new RuntimeException("Cannot have space in " + s);
         }
      }

      String[] tokens = s.split("");
      Stack<Node> stack = new Stack();
      Node node = new Node(null, null, null);
      boolean replacingRoot = false;
      for (int i = 0; i < tokens.length; i++) {
         String token = tokens[i].trim();
         switch (token) {
            case "(":
               if (replacingRoot) {
                  throw new RuntimeException("Trying to replace root in " + s);
               }
               stack.push(node);
               node.firstChild = new Node(null, null, null);
               node = node.firstChild;
               if (tokens[i + 1].trim().equals(",")) {
                  throw new RuntimeException("Comma after node in " + s);
               }
               break;
            case ")":
               node = stack.pop();
               if (stack.size() == 0) {
                  replacingRoot = true;
               }
               break;
            case ",":
               if (replacingRoot) {
                  throw new RuntimeException("Trying to replace root in " + s);
               }
               node.nextSibling = new Node(null, null, null);
               node = node.nextSibling;
               break;
            default:
               if (node.name == null) {
                  node.name = token;
               }
               else {
                  node.name += token;
               }
               break;
         }
      }
      return node;
   }

   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();
      sb.append(this.name);
      if (this.firstChild != null) {
         sb.append("(");
         sb.append(this.firstChild.leftParentheticRepresentation());
         sb.append(")");
      }
      if (this.nextSibling != null) {
         sb.append(",");
         sb.append(this.nextSibling.leftParentheticRepresentation());
      }
      return sb.toString();
   }

   public static void main (String[] param) {
      String s = "(B1,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }

   public String getXmlFormat(int depth) {
      StringBuilder sb = new StringBuilder();
      sb.append("\t".repeat(Math.max(0, depth - 1)));
      sb.append(String.format("<L%s> ", depth));
      sb.append(this.name);

      if (this.firstChild != null && this.nextSibling != null) {
         sb.append("\n");
         depth ++;
         sb.append(this.firstChild.getXmlFormat(depth));
      }

      if (this.firstChild != null && this.nextSibling == null) {
         sb.append("\n");
         depth ++;
         sb.append(this.firstChild.getXmlFormat(depth));
         depth --;
         sb.append("\t".repeat(Math.max(0, depth - 1)));
         sb.append(String.format("</L%s>", depth));
         sb.append("\n");

      }
      if (this.firstChild == null && this.nextSibling != null) {
         sb.append(String.format(" </L%s>", depth));
         sb.append("\n");
         sb.append(this.nextSibling.getXmlFormat(depth));
         return sb.toString();
      }

      if (this.firstChild == null && this.nextSibling == null) {
         sb.append(String.format(" </L%s>", depth));
         sb.append("\n");
      }

      if (this.nextSibling != null) {
         depth --;
         sb.append("\t".repeat(Math.max(0, depth - 1)));
         sb.append(String.format("</L%s>", depth));
         sb.append("\n");
         sb.append(this.nextSibling.getXmlFormat(depth));
      }

      return sb.toString();
   }
}

